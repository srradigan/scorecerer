export type JwtClaimScopes = 'https://c2games.org/jwt/claims' | 'https://hasura.io/jwt/claims';

export type NullableEventRole = EventRole | null | undefined;
export enum EventRole {
  SuperAdmin = 'admin',
  EventAdmin = 'event_admin',
  CTFAdmin = 'ctf_admin',
  Attacker = 'attacker',
  Staff = 'staff',
  Participant = 'participant',
  Advisor = 'advisor',
}
