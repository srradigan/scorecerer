import { KeycloakInterface } from '@auth/keycloak';
import { keycloak as keycloakStore, team as teamStore, eventId as eventIdStore } from '@stores';
import { goto, beforeNavigate } from '$app/navigation';
import { EventRole, type NullableEventRole } from './models';
import type { Unsubscriber } from 'svelte/types/runtime/store';
import { Gql } from '@db';
import { EventCfgByIdQuery } from '@models/EventConfigurations';
import type { EventConfiguration } from '@models/EventConfigurations';
import type { Team } from '@models/Team';

let keycloak: KeycloakInterface | undefined = undefined;
let team: Team | undefined = undefined;
let eventId: string;

// todo get this into a svelte component so we can use $team
keycloakStore.subscribe((data) => (keycloak = data));
teamStore.subscribe((data) => (team = data));
eventIdStore.subscribe((data) => (eventId = data));

export function loginWithRedirect(): Promise<void> {
  return (keycloak ?? new KeycloakInterface()).login(window.location.href);
}

export function redirectToUnauthorized(): Promise<void> {
  return goto(`/error/unauthorized?referer=${window.location.pathname}`);
}

let KeycloakAuthRequiredSubscription: Unsubscriber | null = null;
export async function requireAuth() {
  if (!keycloak?.authenticated) await loginWithRedirect();

  if (!KeycloakAuthRequiredSubscription) {
    // If authenticated, we need to keep it that way
    // Subscribe to updates for the purpose of redirecting to login page
    KeycloakAuthRequiredSubscription = keycloakStore.subscribe(async (data) => {
      if (!data?.authenticated) await loginWithRedirect();
    });

    // The next page might not require auth, so stop this subscription
    beforeNavigate(() => {
      if (!KeycloakAuthRequiredSubscription) return;

      KeycloakAuthRequiredSubscription();
      KeycloakAuthRequiredSubscription = null;
    });
  }
}

export async function requireEventStarted() {
  if (keycloak?.isStaff()) return;
  const event = (await Gql.query<{ events_by_pk: EventConfiguration }>(EventCfgByIdQuery(eventId))).events_by_pk;
  if (!event?.start) {
    console.warn('event start date not found, requireEventStarted is not redirecting');
    return;
  }

  const eventStart = new Date(event.start);
  if (eventStart > new Date()) {
    console.warn('event is not open, redirecting to login!');
    await goto('/countdown');
  }
}

export async function requireTeam() {
  await requireAuth();
  if (keycloak?.isStaff()) return;
  const isParticipant = keycloak?.isRole(EventRole.Participant);
  if (!isParticipant) {
    console.warn('user is not a participant, redirecting unauthorized!');
    return goto(`/error/unauthorized?referer=${window.location.pathname}`);
  }
  if (!team) {
    console.warn('user does not have team, redirecting unauthorized!');
    return goto(`/error/unauthorized?referer=${window.location.pathname}`);
  }
}

/**
 * Require authentication and a particular role to load the page
 * @param roleOrElement Request Role OR HTMLElement requiring the auth. Element is not used,
 *                      only provided for svelte 'use:' compatibility. Will be remapped to required Role if a string.
 * @param requiredRole Role required if element is specified as first parameter
 */
async function requireRole(
  roleOrElement: HTMLElement | EventRole,
  requiredRole: NullableEventRole = null,
): Promise<void> {
  console.log(roleOrElement, requiredRole);
  if (typeof roleOrElement === 'string') requiredRole = roleOrElement;
  // First of all, auth is required (and requireAuth watches for changes to auth)
  await requireAuth();
  // Then check for role
  if (!requiredRole || !keycloak?.isRole(requiredRole)) {
    await redirectToUnauthorized();
  }
}

export default requireRole;
