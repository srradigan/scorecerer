import { UserTeamByEventId } from '@models/Team';
import Env from '../env';
import type { KeycloakTokenParsed } from 'keycloak-js';
import Keycloak from 'keycloak-js';
import { EventRole } from '@auth/models';
import { eventId as eventIdStore, keycloak, team } from '@stores';

let eventId = '';
eventIdStore.subscribe((data) => (eventId = data));

/**
 * Class for interfacing with Keycloak.
 * !!WARNING!! Should not be used directly except for initialization! Use the keycloak store.
 * !!WARNING!! Most Errors in this class will cause an infinite reload loop. WE MUST HANDLE ALL ERRORS
 */
export class KeycloakInterface {
  _onAuthSuccess: Array<CallableFunction>;

  _onAuthRefreshSuccess: Array<CallableFunction>;

  _onAuthError: Array<CallableFunction>;

  _onAuthRefreshError: Array<CallableFunction>;

  _onTokenExpire: Array<CallableFunction>;

  keycloak?: Keycloak;

  constructor() {
    this._onAuthSuccess = [];
    this._onAuthRefreshSuccess = [];
    this._onAuthError = [];
    this._onAuthRefreshError = [];
    this._onTokenExpire = [];
  }

  /*
   * Convenience Getters
   */
  public get authenticated(): boolean {
    return this.keycloak?.authenticated || false;
  }

  public get token(): string | undefined {
    return this.keycloak?.token;
  }

  public get tokenParsed(): KeycloakTokenParsed | undefined {
    return this.keycloak?.tokenParsed;
  }

  /**
   * Return a claim if available, otherwise return defaultValue
   * @param scope Scope of claim - usually the URI of scope owner
   * @param claim Claim to retrieve from the scope
   * @param defaultValue Optional default value
   */
  public getClaim<Claim = string, Default = undefined>(
    scope: string,
    claim: string,
    defaultValue: Default | undefined = undefined,
  ): Claim | Default {
    return this.tokenParsed?.[scope]?.[claim] || defaultValue;
  }

  // Helper to get user id (uuid4) from token
  public get userID(): string | undefined {
    return this.keycloak?.subject;
  }

  // Some helpers for getting the values out of the gnarly claims objects
  public get userEmail(): string | undefined {
    return this.getClaim('https://hasura.io/jwt/claims', 'x-hasura-user-email');
  }

  public get hasuraRoles(): Array<string> | null {
    // return a copy of the array here to prevent funny business
    return [...this.getClaim<string[], never[]>('https://hasura.io/jwt/claims', 'x-hasura-allowed-roles', [])];
  }

  public get hasuraDefaultRole(): string | null {
    return this.getClaim('https://hasura.io/jwt/claims', 'x-hasura-default-role');
  }

  public get eventRoles(): EventRole[] | undefined {
    // return a copy of the array here to prevent funny business
    return [...this.getClaim<EventRole[], never[]>('https://c2games.org/jwt/claims', 'x-hackathon-event-roles', [])];
  }

  async login(redirectUri?: string) {
    if (!this.keycloak) await this.setupKeycloak();
    // this.setupKeycloak asserts that this.keycloak will exist
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return this.keycloak!.login({
      redirectUri: redirectUri || window.location.href,
    });
  }

  logout(redirectUri?: string) {
    return this.keycloak?.logout({
      redirectUri: redirectUri || window.location.origin,
    });
  }

  /**
   * Update Keycloak store when we retrieve new Keycloak information
   */
  async updateInfo() {
    // update our Svelte store
    keycloak.set(this);

    // Return now if we don't have a user ID we can use to fetch our team
    if (!this.userID) return;

    // if our token updates, make sure our team is up-to-date
    try {
      team.set(await UserTeamByEventId(eventId, this.userID));
    } catch (err) {
      console.error('failed to get team by id', err);
    }
  }

  async setupKeycloak(): Promise<void> {
    if (!this.keycloak) {
      this.keycloak = new Keycloak({
        url: Env.KeycloakUrl,
        realm: Env.KeycloakRealm,
        clientId: Env.KeycloakClientID,
      });
    }

    const authenticated = await this.keycloak.init({
      onLoad: 'check-sso',
      silentCheckSsoFallback: true,
      silentCheckSsoRedirectUri: `${window.location.origin}/ssoRedirect`,
    });

    if (authenticated) console.log('Keycloak: authenticated as ', this.tokenParsed, this);
    else console.debug('Keycloak: not authenticated', this);

    await this.updateInfo(); // await this, so we have time to get the Team from the DB

    if (authenticated) {
      // Set an interval to automatically refresh the token. This checks every 30 seconds
      // for if the token will expire within the next 45 seconds
      setInterval(async () => {
        try {
          console.debug('Keycloak: Attempting to update keycloak token');
          const refreshed = await this.keycloak?.updateToken(45);
          if (refreshed) console.debug('Keycloak: Token was successfully refreshed');
          else console.debug('Keycloak: Token is still valid');
        } catch (err) {
          console.error(`Keycloak: Failed to refresh the token, or the session has expired ${err}`);
          // Clear login status in components and re-display login button to user
          void this.updateInfo();
        }
      }, 30000);
    }

    // setup our listeners
    this.keycloak.onAuthSuccess = this.doAuthSuccess.bind(this);
    this.keycloak.onAuthRefreshSuccess = this.doAuthRefreshSuccess.bind(this);
    this.keycloak.onAuthError = this.doAuthError.bind(this);
    this.keycloak.onAuthRefreshError = this.doAuthRefreshError.bind(this);
    this.keycloak.onTokenExpired = this.doTokenExpire.bind(this);

    // Keycloak interface
    this.onAuthSuccess(this.updateInfo.bind(this));
    this.onAuthRefreshSuccess(this.updateInfo.bind(this));
    this.onTokenExpire(this.updateInfo.bind(this));
  }

  onAuthSuccess(cb: () => void) {
    this._onAuthSuccess.push(cb);
  }

  onAuthRefreshSuccess(cb: () => void) {
    this._onAuthRefreshSuccess.push(cb);
  }

  onAuthError(cb: () => void) {
    this._onAuthError.push(cb);
  }

  onAuthRefreshError(cb: () => void) {
    this._onAuthRefreshError.push(cb);
  }

  onTokenExpire(cb: () => void) {
    this._onTokenExpire.push(cb);
  }

  // stand in functions to use for keycloak callbacks
  private doAuthSuccess(...args: unknown[]) {
    this._onAuthSuccess.forEach((cb) => cb(...args));
  }

  private doAuthRefreshSuccess(...args: unknown[]) {
    this._onAuthRefreshSuccess.forEach((cb) => cb(...args));
  }

  private doAuthError(...args: unknown[]) {
    this._onAuthError.forEach((cb) => cb(...args));
  }

  private doAuthRefreshError(...args: unknown[]) {
    this._onAuthRefreshError.forEach((cb) => cb(...args));
  }

  private doTokenExpire(...args: unknown[]) {
    this._onTokenExpire.forEach((cb) => cb(...args));
  }

  isRole(role: EventRole, allowSuper = true): boolean {
    if (allowSuper && this.eventRoles && this.eventRoles.indexOf(EventRole.SuperAdmin) !== -1) return true;
    return this.eventRoles ? this.eventRoles.indexOf(role) !== -1 : false;
  }

  getFirstRole(roles: EventRole[], allowSuper = true): EventRole | null {
    if (allowSuper) roles = [EventRole.SuperAdmin, ...roles];
    for (const role of roles) {
      if (this.isRole(role)) return role;
    }
    return null;
  }

  isStaff(): boolean {
    return !!this.getFirstRole([EventRole.CTFAdmin, EventRole.EventAdmin, EventRole.Attacker, EventRole.Staff]);
  }

  getHighestPrivilegeRole(): EventRole | null {
    return this.getFirstRole([
      EventRole.SuperAdmin,
      EventRole.EventAdmin,
      EventRole.CTFAdmin,
      EventRole.Attacker,
      EventRole.Staff,
      EventRole.Participant,
      EventRole.Advisor,
    ]);
  }
}
