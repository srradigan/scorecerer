/* eslint-disable */
/* prettier formatting applied but this needs to be revisited to deal with eslint issues */
import Highcharts from 'highcharts';
import DarkUnica from 'highcharts/themes/dark-unica';

export default (node, config) => {
  const redraw = true;
  const oneToOne = true;
  DarkUnica(Highcharts);
  const chart = Highcharts.chart(node, config);

  return {
    update(config) {
      chart.update(config, redraw, oneToOne);
    },
    destroy() {
      chart.destroy();
    },
  };
};
