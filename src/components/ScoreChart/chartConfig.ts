/* eslint-disable */
/* disabling eslint for this file because I just cant deal with this file right now.. maybe later */
import * as Highcharts from 'highcharts';

const SCOREBOARD_CATEGORIES = ['Service', 'CTF', 'Other'];
const SCOREBOARD_CAT_TO_DATA = {
  Service: 'service',
  CTF: 'ctf',
  Other: 'bonus',
};

export function configureChart(series, teams) {
  return {
    colors: '#2A9fd6 #cf2027 #d5e0f2 #7798BF #aaeeee #ff0066 #eeaaee #55BF3B #DF5353 #7798BF #aaeeee'.split(' '),
    chart: {
      type: 'column',
    },
    title: { text: 'Team Scores' },
    subtitle: { text: '(Updates automatically)' },
    // xAxis is the team names along the bottom
    xAxis: {
      type: 'category',
      categories: teams,
      labels: {
        autoRotation: [-45, -90],
        style: {
          fontSize: '1.2em',
          fontWeight: 'bold',
          textOutline: '1px contrast',
        },
      },
      tickWidth: 0,
      crosshair: false,
    },
    // yAxis is the left-side Point values
    yAxis: {
      min: 0,
      title: {
        text: 'Points',
      },
      // yAxis.stackLabels is the top-of-the-bar point total for each team
      stackLabels: {
        enabled: true,
        // Fixes scores above columns from disappearing
        allowOverlap: true,
        // Fixes the 'bad spacing' of scores over 1000 (otherwise rendered as '1 000')
        formatter() {
          return this.total;
        },
        style: {
          color: Highcharts.theme?.contrastTextColor || 'white',
          fontSize: '3.0em',
        },
      },
    },
    // plotOptions.column is the bars themselves, with embedded point labels
    // (the ones that are always off-center)
    plotOptions: {
      column: {
        stacking: 'normal',
        pointPadding: 0,
        groupPadding: 0.1,
        shadow: true,
        dataLabels: {
          enabled: true,
          formatter() {
            return this.y;
          },
          style: {
            fontSize: '2.5em',
          },
        },
      },
    },
    // Legend is a floating box on the top-right of the chart
    legend: {
      floating: true,
      align: 'right',
      verticalAlign: 'top',
      x: -30,
      y: 25,
      backgroundColor: '#505053',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: true,
    },
    // Tooltip is the box that appears when hovering over a specific column
    tooltip: {
      shared: true,
      useHTML: true,
      headerFormat: '<span>{point.key}</span><table style="background-color:initial">',
      pointFormat:
        '<tr>' +
        '<td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.0f}</b></td>' +
        '</tr>',
      footerFormat: '</table>',
      hideDelay: 100,
      style: {
        fontSize: '2em',
      },
    },
    responsive: {
      rules: [
        {
          condition: { maxWidth: 720 },
          chartOptions: {
            ...hc_yax_bar_top_fsize('2.5em'),
            ...hc_yax_cat_pts_fsize('2em'),
            yAxis: { title: { text: null } },
          },
        },
        {
          condition: { maxWidth: 600 },
          chartOptions: {
            ...hc_yax_bar_top_fsize('1.9em'),
            ...hc_yax_cat_pts_fsize('1.3em'),
            ...hc_xax_fsize('0.7em'),
            legend: {
              floating: false,
              verticalAlign: 'bottom',
              align: 'center',
              x: 0,
              y: 0,
            },
          },
        },
        {
          condition: { maxWidth: 400 },
          chartOptions: {
            ...hc_yax_bar_top_fsize('1.0em'),
            ...hc_yax_cat_pts_fsize('0.8em'),
          },
        },
      ],
    },
    series,
  };
}

export function build_hc_series(scores, categories = SCOREBOARD_CATEGORIES) {
  return categories.map((cat) => ({
    id: cat,
    name: cat,
    data: scores.map((e) => parseInt(e[SCOREBOARD_CAT_TO_DATA[cat]], 10)),
  }));
}

function hc_yax_bar_top_fsize(em) {
  return { yAxis: { stackLabels: { style: { fontSize: em } } } };
}

function hc_yax_cat_pts_fsize(em) {
  return {
    plotOptions: { column: { dataLabels: { style: { fontSize: em } } } },
  };
}

function hc_xax_fsize(em) {
  return { xAxis: { labels: { style: { fontSize: em } } } };
}
