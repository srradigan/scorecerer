// disable server side rendering on all pages,
// the keycloak library doesn't support it well
export const ssr = false;
