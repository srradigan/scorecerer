import type { AxiosResponse, Method } from 'axios';
import axios from 'axios';
import ENV from '../env';
import type { KeycloakInterface } from '@auth/keycloak';
import { keycloak as keycloakStore } from '@stores';
import { EventRole } from '@auth/models';
import type { NullableEventRole } from '@auth/models';
import type { ProcessedFile } from '@models/Storage';
import Env from '../env';

type StorageApiHeaders = {
  Authorization?: string;
  'x-hasura-role'?: EventRole;
  'Content-Type'?: string;
};

export type PreSignedUrlResponse = {
  url: string;
  expiration: number;
};

export type StorageApiResponse = {
  processedFiles: ProcessedFile[];
};

let keycloak: KeycloakInterface | undefined;
keycloakStore.subscribe((value) => (keycloak = value));

const axiosAPI = axios.create({
  baseURL: ENV.StorageApi,
});

function getHeaders(role: NullableEventRole): StorageApiHeaders {
  const headers: StorageApiHeaders = {
    Authorization: `Bearer ${keycloak?.token}`,
  };
  if (role) headers['x-hasura-role'] = role;
  return headers;
}

export async function storageApiRequest<T>(
  method: Method,
  url: string,
  data?: string | FormData | Record<string, unknown>,
): Promise<AxiosResponse<T>> {
  const headers = getHeaders(keycloak?.getFirstRole([EventRole.SuperAdmin, EventRole.CTFAdmin, EventRole.Participant]));
  return axiosAPI({ method, url, data, headers });
}

export async function getStorageFilePreSignedUrl(fileId: string): Promise<string> {
  const response = await storageApiRequest<PreSignedUrlResponse>('GET', `/v1/files/${fileId}/presignedurl`);
  // We might know the server's "external address" better than it does
  // so replace the server's base URL with our version of it
  const responseUrl = new URL(response.data.url);
  console.debug('original URL', new URL(response.data.url));
  let externalHostUrl;

  if (Env.StorageS3API) {
    // If an external Storage S3 URL is defined, we'll generate links to use S3 directly
    externalHostUrl = new URL(Env.StorageS3API);
    responseUrl.pathname = `/${Env.StorageBucket}/${fileId}`;
  } else {
    // Otherwise, we'll generate links using the storage API URL
    externalHostUrl = new URL(Env.StorageApi);
  }

  responseUrl.host = externalHostUrl.host;
  responseUrl.protocol = externalHostUrl.protocol;
  responseUrl.port = externalHostUrl.port;

  console.debug('generated URL', responseUrl);
  return responseUrl.href;
}

export function postFiles<T = StorageApiResponse>(files: FileList): Promise<AxiosResponse<T>> {
  const data = new FormData();
  for (const file of files) {
    data.append('metadata[]', new Blob([JSON.stringify({ name: file.name })], { type: 'application/json' }));
    data.append('file[]', file);
  }

  const headers = getHeaders(keycloak?.getHighestPrivilegeRole());
  return axiosAPI({ method: 'POST', url: '/v1/files/', headers, data });
}
