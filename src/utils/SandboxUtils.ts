import type { PveConfig } from '@models/Sandbox';

export function generateVmLink(pveConfig: PveConfig, vmid: number | string, type: 'qemu' | 'lxc' = 'qemu') {
  // https://sandbox.c2games.org:8006/#v1:0:=qemu/1197
  return `${pveConfig.url}/#v1:0:=${type}/${vmid}`;
}
