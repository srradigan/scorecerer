import { writable, type Writable } from 'svelte/store';
import type { KeycloakInterface } from '@auth/keycloak';
import type { EventConfiguration } from '@models/EventConfigurations';
import type { Team } from '@models/Team';
import Env from '@env';

// Export a Svelte store for other elements to subscribe to
export const keycloak: Writable<KeycloakInterface> = writable();
export const team: Writable<Team> = writable();
export const eventCfg: Writable<EventConfiguration> = writable();
export const eventId: Writable<string> = writable(Env.EventID);
