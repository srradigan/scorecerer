type Environment = {
  AssetStorage: string;
  GqlPublicUri: string;
  SandboxApiUri: string;
  TapiUri: string;
  KeycloakUrl: string;
  KeycloakRealm: string;
  KeycloakClientID: string;
  EventID: string;
  StorageApi: string;
  StorageBucket: string;
  StorageS3API: string;
};

const ENV: Environment = {
  AssetStorage: import.meta.env.VITE_ASSET_STORAGE_URI as string,
  GqlPublicUri: import.meta.env.VITE_GQL_PUBLIC_URI as string,
  TapiUri: import.meta.env.VITE_TAPI_URI as string,
  KeycloakUrl: import.meta.env.VITE_KEYCLOAK_URL as string,
  KeycloakRealm: import.meta.env.VITE_KEYCLOAK_REALM as string,
  KeycloakClientID: import.meta.env.VITE_KEYCLOAK_CLIENT_ID as string,
  SandboxApiUri: import.meta.env.VITE_SANDBOX_API_URI as string,
  EventID: import.meta.env.VITE_EVENT_ID as string,
  StorageApi: import.meta.env.VITE_STORAGE_API as string,
  StorageBucket: import.meta.env.VITE_STORAGE_BUCKET as string,
  StorageS3API: import.meta.env.VITE_STORAGE_S3_API as string,
};

declare global {
  interface Window {
    C2GamesConfig: Environment;
  }
}

console.debug('ENV: ', ENV);
export default ENV;
