import { error } from '@sveltejs/kit';
import { getBundleById, getEnvironmentById } from '@utils/SandboxApi';

export async function load({ params }: { params: { environmentId: string } }) {
  const environmentId = params.environmentId;
  try {
    const environment = await getEnvironmentById(environmentId);
    const bundle = await getBundleById(environment.bundle);
    return {
      environmentId,
      environment,
      bundle,
    };
  } catch (e) {
    console.error(e);
    throw error(404, `Environment Not found: ${environmentId}`);
  }
}
