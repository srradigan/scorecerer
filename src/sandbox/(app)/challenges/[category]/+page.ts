import type { BundleCategory } from '@models/Sandbox';
import type { KeycloakInterface } from '@auth/keycloak';

type PageParameters = {
  params: { category: BundleCategory };
  context: { keycloak: KeycloakInterface };
};

export async function load({ params }: PageParameters) {
  return {
    category: params.category,
  };
}
