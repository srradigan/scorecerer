import type { Team } from './Team';

export type Scores = {
  ctf: string;
  bonus: string;
  service: string;
  total: string;
  team: Partial<Team>;
};

export type ScoreResponse = {
  scores: Array<Scores>;
};

export function ScoresQuery(eventId: string) {
  return `{
    scoreboard_team_summary(
      order_by: {order: asc},
      where: {event_id: {_eq: ${JSON.stringify(eventId)}}}
    ) {
      score {
        bonus
        ctf
        event_id
        service
        team_id
        total
        team {
          id
          name
          long_name
        }
      }
    }
  }`;
}
