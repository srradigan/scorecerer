export type C2GamesClaims = {
  'x-hackathon-event-roles': Array<string>;
  'x-hackathon-event-team-id': number;
};

export type HasuraClaims = {
  'x-hasura-allowed-roles': Array<string>;
  'x-hasura-default-role': string;
  'x-hasura-event-team-id': string;
};
