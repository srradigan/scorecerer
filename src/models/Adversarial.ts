import type { CheckResultRawExecData, FeedbackDetails } from '@models/Service';

export type ScoreBoardAdversarialStatus = {
  team_id: string;
  event_id: string;
  check_id: string;

  check_time: string;
  result_code: string;
  visible: boolean;
  participant?: FeedbackDetails;
  staff?: FeedbackDetails;
  data?: CheckResultRawExecData;

  last_success: string;
  last_partial: string;
  last_failure: string;
  last_error: string;

  check_order: number;
  team_order: number;
};
