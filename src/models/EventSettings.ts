import { Gql } from '../db';

export type Event = {
  id?: string;
  name?: string;
  start?: string;
  end?: string;
};
export type EventSettings = {
  event?: Event;
  event_id?: string;
  org_name?: string;
  org_link?: string;
  info_link?: string;
};

export type EventSettingsReturn = {
  scoreboard_event_settings: Array<EventSettings>;
};

export const AllEventSettingsQuery = `{
    scoreboard_event_settings(order_by: {event_id: asc}) {
      event {
        id
        name
        start
        end
      }
      info_link
      org_link
      org_name
    }
  }`;

export function EventSettingsQuery(eventId: string) {
  return `{
      scoreboard_event_settings(
        where: {
          event_id: {_eq: ${JSON.stringify(eventId)}}
        }
      ) {
        event {
          id
          name
          start
          end
        }
        info_link
        org_link
        org_name
      }
    }`;
}

export async function GetEventSettings(eventId: string): Promise<EventSettings> {
  const response = await Gql.query<EventSettingsReturn>(`query ${EventSettingsQuery(eventId)}`);
  const values = response.scoreboard_event_settings;
  if (values.length < 1) {
    throw `no event found with id '${eventId}'`;
  }
  return values[0];
}
