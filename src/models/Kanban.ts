import type { Challenge } from './Challenges';
import type { ServiceStatus } from './Service';
import { Gql } from '../db';

export type KanbanTask = {
  id?: string;
  event_id: string;
  check_id?: string;
  ctf_challenge_id?: string;
  team_id?: string;
  assignee: string;
  title: string;
  description: string;
  status: string;
  created_by: string;
  created_at?: string; // todo needed?
  ctf_challenge?: Challenge;
  service_status?: ServiceStatus;
};

export const KanbanFields = `
    assignee
    check_id
    created_at
    description
    id
    status
    title
    created_by
    ctf_challenge {
      challenge {
        name
        description
      }
    }`;

export const KanbanQuery = `{
   scoreboard_kanban {
    ${KanbanFields}
  }
}`;

export async function createKanbanTask(task: KanbanTask): Promise<{ id: string }> {
  return await Gql.query(`mutation CreateKanbanTask {
      insert_scoreboard_kanban_one(object: {
        assignee: ${JSON.stringify(task.assignee)},
        created_by: ${JSON.stringify(task.created_by)}, 
        title: ${JSON.stringify(task.title)}, 
        description: ${JSON.stringify(task.description)}, 
        status: ${JSON.stringify(task.status)}, 
        team_id: ${JSON.stringify(task.team_id)}, 
        event_id: ${JSON.stringify(task.event_id)},
        ctf_challenge_id: ${JSON.stringify(task.ctf_challenge_id || null)},
        check_id: ${JSON.stringify(task.check_id || null)},
        created_at: ${JSON.stringify(task.created_at || new Date().toISOString())},
      }) {
        ${KanbanFields}
      }
    }`);
}

export async function updateTaskAssignee(taskId: string, assignee: string) {
  await Gql.query(`mutation UpdateKanbanAssignee {
      update_scoreboard_kanban_by_pk(
        pk_columns: { id: ${JSON.stringify(taskId)} },
        _set: { assignee: ${JSON.stringify(assignee)} }
      ) {
        id
      }
    }`);
}
