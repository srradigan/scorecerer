import type { ActiveChallenge, Challenge } from '@models/Challenges';
import type { EventConfiguration } from '@models/EventConfigurations';
import type { StorageFile } from '@models/Storage';

export type ChallengeFile = {
  event_id: string;
  challenge_id: string;
  file_id: string;

  active?: ActiveChallenge;
  challenge?: Challenge;
  event?: EventConfiguration;
  file?: StorageFile;
};
