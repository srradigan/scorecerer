import { Gql } from '../db';
import type { QueryOptions } from '../db';
import type { ChallengeFile } from '@models/Challenges.Files';

export function InsertChallengeFile(file: ChallengeFile, queryOptions?: QueryOptions) {
  const query = `mutation {
    insert_challenges_files_one(object: {
      file_id: ${JSON.stringify(file.file_id)},
      event_id: ${JSON.stringify(file.event_id)},
      challenge_id: ${JSON.stringify(file.challenge_id)}
    }) {
      file_id
      event_id
      challenge_id
      file {
        id
        name
        size
        mimeType
        createdAt
        updatedAt
      }
    }
  }`;
  return Gql.query<{ insert_challenges_files_one: ChallengeFile }>(query, queryOptions);
}
