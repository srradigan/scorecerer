import type { Team, TeamSettings } from '@models/Team';
import { Gql } from '@db';

export const DeleteTeamMutation = `
mutation deleteTeam($team_id: uuid) {
  delete_scoring_bonus_points(where: {team_id: {_eq: $team_id}}) {
    affected_rows
  }
  delete_checks_deployed(where: {team_id: {_eq: $team_id}}) {
    affected_rows
  }
  delete_checks_results(where: {team_id: {_eq: $team_id}}) {
    affected_rows
  }
  delete_scoreboard_team_settings(where: {team_id: {_eq: $team_id}}) {
    affected_rows
  }
  delete_challenges_submissions(where: {team_id: {_eq: $team_id}}) {
    affected_rows
  }
  delete_variables_team(where: {team_id: {_eq: $team_id}}) {
    affected_rows
  }
  delete_event_registration(where: {team_id: {_eq: $team_id}}) {
    affected_rows
  }
  delete_team_membership(where: {team_id: {_eq: $team_id}}) {
    affected_rows
  }
  delete_teams(where: {id: {_eq: $team_id}}) {
    affected_rows
  }
}`;

export function TeamSettingsQuery(eventId?: string) {
  let q = `{
    scoreboard_team_settings(
      order_by: {order: asc}`;
  q += eventId ? `, where: {event_id: {_eq: ${JSON.stringify(eventId)}}}` : '';
  q += `) {
      visible
      team_id
      team {
        id
        name
        long_name
      }
      event_id
      event {
        start
        name
        id
        end
      }
    }
  }`;
  return q;
}

export async function GetAllEnabledBlueTeams(eventId: string): Promise<Team[]> {
  const q = TeamSettingsQuery(eventId);

  const response = await Gql.query<{ scoreboard_team_settings: TeamSettings[] }>(q);
  return response.scoreboard_team_settings.map((e) => e.team);
}
