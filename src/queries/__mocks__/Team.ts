/* eslint-disable */
import { BlueTeam } from '../../../test/jest/utils/fixtures';

export async function GetAllEnabledBlueTeams() {
  return BlueTeam[0];
}

export async function getScoreboardTeams() {
  return BlueTeam;
}
