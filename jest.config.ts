// Or async function
export default async () => {
  return {
    verbose: true,
    testEnvironment: 'jsdom',
    transform: {
      '^.+\\.svelte?$': ['svelte-jester', { preprocess: true }],
      '^.+\\.ts$': 'ts-jest',
    },
    moduleFileExtensions: ['js', 'svelte', 'ts'],
    reporters: ['default', 'jest-junit'],
  };
};
