/* eslint-disable */
import { act, render, screen } from '@testing-library/svelte';

import BlueteamSelect from '../../../src/components/Utils/BlueteamSelect.svelte';
import { AdminTeam, BlueTeam } from '../utils/fixtures';

jest.mock('../../../src/models/Team');
jest.mock('../../../src/queries/Team');
jest.mock('../../../src/db');
jest.mock('../../../src/keycloak');

describe('Test BlueTeamSelect', () => {
  test('Renders select', () => {
    render(BlueteamSelect, { props: { blueteams: BlueTeam } });

    expect(screen.getByText('Select team')).not.toBeNull();
  });
  test('Renders select and selects a team', () => {
    render(BlueteamSelect, { props: { blueteams: BlueTeam } });
    const button = screen.getByText('Select team');
    act(() => {
      button.click();
    });
    const team1 = screen.getByText('team1');
    act(() => {
      team1.click();
    });
    expect(screen.getByText('Select team')).not.toBeNull();
  });
});
