/* eslint-disable */
import { render, screen } from '@testing-library/svelte';
import Feedback from '../../../src/components/ScoreboardFeedback/Feedback.svelte';

const testJson = {
  feedback: 'super great amazing feedback',
  detailed_results: 'extra detailed details detailing detailed tails',
};
test('renders feedback', () => {
  render(Feedback, { props: { feedback: testJson } });

  expect(screen.queryByText('super great amazing feedback')).not.toBeNull();
  //expect(screen.queryByText("extra detailed details detailing detailed tails")).not.toBeNull();
});
