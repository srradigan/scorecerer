export const BlueTeam = [
  {
    id: 123,
    name: 'team1',
    role_name: 'blueteam',
    disabled: false,
    visible: true,
  },
  {
    id: 124,
    name: 'team2',
    role_name: 'blueteam',
    disabled: false,
    visible: true,
  },
];

export const AdminTeam = {
  disabled: false,
  id: 10001,
  name: 'Orangeteam',
  role_name: 'admin',
  visible: false,
};
