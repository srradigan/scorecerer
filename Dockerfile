FROM node:18-alpine

WORKDIR /usr/src/app

RUN apk update
RUN apk add git

# install deps
COPY package.json .
RUN npm install

# copy app
COPY static/ /usr/src/app/static
COPY src/ /usr/src/app/src

CMD npm run dev -- --host 0.0.0.0
