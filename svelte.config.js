import adapter from '@sveltejs/adapter-auto';
import preprocess from 'svelte-preprocess';

// eslint-disable-next-line no-undef
const SANDBOX_BUILD = !!process.env.SANDBOX_BUILD;

/** @type {import('@sveltejs/kit').Config} */
const config = {
  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  preprocess: [
    preprocess({
      scss: {
        prependData: '@use "src/variables.scss" as *;',
      },
    }),
  ],

  kit: {
    adapter: adapter(),
    files: {
      routes: SANDBOX_BUILD ? 'src/sandbox' : 'src/routes',
    },
    alias: {
      '@models/*': 'src/models',
      '@components/*': 'src/components',
      '@queries/*': 'src/queries',
      '@auth/*': 'src/auth',
      '@utils/*': 'src/utils',
      '@stores': 'src/stores.ts',
      '@db': 'src/db.ts',
      '@env': 'src/env.ts',
    },
  },
};

export default config;
